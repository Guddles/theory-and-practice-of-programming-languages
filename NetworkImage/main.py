import matplotlib.pyplot as plt
import numpy as np
import socket
from math import *

host = "84.237.21.36"
port = 5152
packet_size = 40002


def recvall(sock, n):
    data = bytearray()
    while len(data) < n:
        packet = sock.recv(n-len(data))
        if not packet:
            return
        data.extend(packet)
    return data

plt.ion()
plt.figure()
def dist(coord1, coord2):
    return sqrt(pow(coord1[0]-coord2[0], 2) + pow(coord1[1]-coord2[1], 2))

def maximums(img):
    _max = []
    for i in range(1, len(img)-1):
        for j in range(1, len(img[i])-1):
            if img[i - 1][j] < img[i][j] and img[i + 1][j] < img[i][j] and img[i][j - 1] < img[i][j] and img[i][j + 1] < img[i][j]:

                _max.append((i,j))
    return _max
imgs=0
with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.connect((host, port))
    while imgs<=10:
        sock.send(b"get")
        bts = recvall(sock, packet_size)
        # print(len(bts))
        rows ,cols = bts[:2]
        # print(rows, cols)
        im1 = np.frombuffer(bts[2:rows*cols+2], dtype="uint8").reshape(rows, cols)
        
        # rows2, cols2 = bts[rows*cols+2:rows*cols+4]
        # im2 = np.frombuffer(bts[rows*cols+4:], dtype="uint8").reshape(rows2,cols2)
        
        pos1 = np.unravel_index(np.argmax(im1),im1.shape)
        # pos2 = np.unravel_index(np.argmax(im2),im2.shape)
        # diff = np.abs(np.array(pos1)-np.array(pos2))
        coords = maximums(im1)
        _dist = 0
        if len(coords)>=2:
            _dist = round(dist(coords[0], coords[1]),1)
        plt.imshow(im1)
        plt.pause(2)
        print(_dist)
        
        sock.send(str(_dist).encode())
        bts = sock.recv(20)
        print(bts)
        if bts == b'yep':
            imgs+=1
    
    # plt.clf()
    # plt.subtitle(str(diff))
    # plt.subplot(121)
    # plt.title(str(pos1))
    # plt.imsow(im1)
    # plt.subplot(122)
    # plt.title(str(pos2))
    # plt.imshow(im2)
    # plt.pause(2)
    
print("Done")            