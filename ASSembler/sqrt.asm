%macro pushd 0
    push edx
    push ecx
    push ebx
    push eax
%endmacro

%macro popd 0
    pop eax
    pop ebx
    pop ecx
    pop edx
%endmacro

%macro print 2
    pushd
    mov edx, %1
    mov ecx, %2
    mov ebx, 1
    mov eax, 4
    int 0x80
    popd
%endmacro

%macro dprint 0
    pushd
    
    mov ecx, 10
    mov bx, 0
    %%_divide:
        mov edx, 0
        div ecx
        push dx
        inc bx
        test eax, eax
        jnz %%_divide
        mov cx, bx
    %%_digit:
        pop ax
        add ax, '0'
        mov [count], ax
        print 1, count
        dec cx
        mov ax, cx
        cmp cx, 0
        jg %%_digit
    popd
%endmacro

%macro calc 2
    push edx
    push ecx
    push ebx
    
    xor edx, edx
    mov ecx, %2
    push ecx
    
    mov eax, %1
    div ecx

    pop edx
    
    add eax, edx
    xor edx, edx
    mov ecx, 2
    div ecx
    
    pop edx
    pop ecx
    pop ebx 

%endmacro



section .text

global _start

_start:
    mov eax, [number]
    
    cmp eax, 0
    
    mov ecx, 2 
    div ecx
    mov edx, 0
    mov [mem1], eax 

    calc [number], eax
    mov [mem2], eax
    
_loop:
    mov eax, [mem1]
    mov ecx, [mem2]
    sub eax, ecx
    
    cmp eax, 1
    jl _end

    mov eax, [mem2]
    mov [mem1], eax
    calc [number], [mem1]
    mov [mem2], eax
    jmp _loop
    
_end:
    mov eax, [mem2]
    print len, message
    dprint

    mov     eax, 1
    int     0x80
    
section .data
    number dd 121
    message db "Sqrt is: "
    len equ $ - message
    newline db 0xA, 0xD
    nlen equ $ - newline

segment .bss
    count resd 1
    mem1 resd 1
    mem2 resd 1
    result resd 1