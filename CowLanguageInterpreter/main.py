import numpy as np
import re
file = open('hello.cow', 'r')
res=file.read()
rr = re.sub(r'\n', r' ', res)
rr = rr.split(' ')
stack=[]
dic = {}
ind=0
result = np.zeros(1000)

for item in rr:
    if item == 'MOO':
        stack.append(ind)
        print(stack)
    if item == 'moo':
        dic[ind]=stack[len(stack)-1]
        dic[stack.pop()]=ind
    ind+=1
index = 0
i = 0

'''
    MoO - значение текущей ячейки увеличить на 1
    MOo - значение ​текущей ячейки уменьшить на 1
    moO - следующая ячейка
    mOo - предыдущая ячейка
    moo - начало цикла
    MOO - конец цикла
    OOM - вывод значения текущей ячейки
    oom - ввод значения в текущую ячейку
    mOO - выполнить инструкцию с номером из текущей ячейки
    Moo - если значение в ячейке равно 0, то ввести с клавиатуры, если значение не 0, то вывести на экран
    OOO - обнулить значение в ячейке
'''

while(i != len(rr)):
    match rr[i]:
        case 'MoO':
            result[index] += 1
        case 'MOo':
            result[index] -= 1
        case 'mOO':
            result[index] = i
        case 'moO':
            index += 1
        case 'mOo':
            index -= 1
        case 'OOM':
            print(chr(int(result[index])),end='')
        case 'oom':
            result[index] = input()
        case 'Moo':
            if rr[index] != 0:
                print(chr(int(result[index])),end='')
            else:
                input("Введите свое значение")
        case 'OOO':
            rr[index] = 0
        case 'moo':
            i = dic[i]-1
        case 'MOO':
            if result[index] == 0:
                i = dic[i]
    if rr[i] == '':
        pass
    else:
        i += 1
        continue
    i += 1