

class IlocError(message: String?): Exception(message)
class PlocError(message: String?): Exception(message)

fun godDamnItsNumber(str: String): Boolean{
    for (char in str.trim()){
        if (!char.isDigit()){
            return false
        }
    }
    return true
}

class SpecialHashMap : HashMap<String, Int>(){
    val iloc = Iloc()
    val ploc = Ploc()

    inner class Iloc{
        operator fun get(idx: Int): Int{
            val srtKeys = this@SpecialHashMap.keys.toSortedSet().toList()
            if(idx < 0 || idx >= srtKeys.size){
                throw IlocError("Hey, buddie, in your Iloc call is mistake cause that index doesn't exist")
            }
            return this@SpecialHashMap[srtKeys[idx]]!!
        }
    }

    inner class Ploc{
        operator fun get(condit: String): HashMap<String, Int>{
            var result = this@SpecialHashMap.clone() as HashMap<String, Int>
            val conditions = condit.split(',').toMutableList()
            for ((i, condit) in conditions.withIndex()){
                conditions[i] = condit.trim()
            }
            val newResult = HashMap<String, Int>()
            for ((key,value) in result){
                var clearKey = key
                if (clearKey[0] == '('){
                    clearKey = clearKey.slice(1 until clearKey.length)
                }
                if(clearKey[clearKey.length-1] == ')'){
                    clearKey = clearKey.slice(0 until clearKey.length-1)
                }
                val idxs = clearKey.split(',')
                if(idxs.size == conditions.size){
                    var matches = true
                    for (i in idxs.indices){
                        if(godDamnItsNumber(idxs[i])){
                            val number = idxs[i].trim().toInt()

                            val oneCharOp = conditions[i][0]
                            val oneCharVal = conditions[i].slice(1 until conditions[i].length).trim().toIntOrNull()

                            val twoCharOp = conditions[i].slice(0 until 2).trim()
                            val twoCharVal = conditions[i].slice(2 until conditions[i].length).trim().toIntOrNull()

                            if (oneCharVal == null && twoCharVal == null){
                                throw PlocError("Hey, buddie, in your Ploc call is mistake cause bad index")
                            }

                            matches = matches && when(twoCharOp){
                                ">=" -> number >= twoCharVal!!
                                "<=" -> number <= twoCharVal!!
                                "<>" -> number != twoCharVal!!
                                else -> when(oneCharOp){
                                    '>' -> number > oneCharVal!!
                                    '<' -> number < oneCharVal!!
                                    '=' -> number == oneCharVal!!.toInt()
                                    else -> throw  PlocError("Hey, buddie, in your Ploc call is mistake cause your OP is wrong")
                                }
                            }
                        }
                        else{
                            matches = false
                        }
                    }
                    if(matches){
                        newResult[key] = value
                    }
                }
                result = newResult
            }
            return result
        }
    }

}