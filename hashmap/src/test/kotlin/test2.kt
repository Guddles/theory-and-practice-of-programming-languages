import jdk.jshell.spi.ExecutionControlProvider
import org.junit.jupiter.api.*

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import java.lang.Exception
import java.lang.IllegalArgumentException
import java.lang.reflect.Executable

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
internal class PlocTest{
    val map = SpecialHashMap()
    @BeforeAll
    fun PlocMapTest(){
        map["value1"] = 1
        map["value2"] = 2
        map["value3"] = 3
        map["1"] = 10
        map["2"] = 20
        map["3"] = 30
        map["(1, 5)"] = 100
        map["(5, 5)"] = 200
        map["(10, 5)"] = 300
        map["(1, 5, 3)"] = 400
        map["(5, 5, 4)"] = 500
        map["(10, 5, 5)"] = 600
    }
    @Test
    @DisplayName(">=1")
    fun test1(){
        val answ: HashMap<String, Int> = hashMapOf("1" to 10, "2" to 20, "3" to 30)
        assertTrue(map.ploc[">=1"] == answ)
    }
    @Test
    @DisplayName("<3")
    fun test2(){
        val answ: HashMap<String, Int> = hashMapOf("1" to 10, "2" to 20)
        assertTrue(map.ploc["<3"] == answ)
    }@Test
    @DisplayName(">0, >0")
    fun test3(){
        val answ: HashMap<String, Int> = hashMapOf("(1, 5)" to 100, "(5, 5)" to 200, "(10, 5)" to 300)
        assertTrue(map.ploc[">0, >0"] == answ)
    }
    @Test
    @DisplayName(">10, >0")
    fun test4(){
        val answ: HashMap<String, Int> = hashMapOf("(10, 5)" to 300)
        assertTrue(map.ploc[">=10, >0"] == answ)
    }@Test
    @DisplayName("<5, >=5, >=3")
    fun test5(){
        val answ: HashMap<String, Int> = hashMapOf("(1, 5, 3)" to 400)
        assertTrue(map.ploc["<5, >=   5, >=3"] == answ)
    }
    @Test
    @DisplayName("=3")
    fun test6(){
        val answ: HashMap<String, Int> = hashMapOf("3" to 30)
        assertTrue(map.ploc["   =   3 "] == answ)
    }
    @Test
    @DisplayName("<>1")
    fun test7(){
        val answ: HashMap<String, Int> = hashMapOf("2" to 20, "3" to 30)
        assertTrue(map.ploc["   <>1    "] == answ)
    }
    @Test
    @DisplayName("NaN")
    fun test8(){
        val exp: Exception = assertThrows(IlocError::class.java){
            map.iloc[-1]
        }
        assertEquals("Hey, buddie, in your Iloc call is mistake cause that index doesn't exist", exp.message)
    }
    @Test
    @DisplayName("Bad index")
    fun test9(){
        val exp: Exception = assertThrows(PlocError::class.java){
            map.ploc["bruh"]
        }
        assertEquals("Hey, buddie, in your Ploc call is mistake cause bad index", exp.message)
    }
    @Test
    @DisplayName("Bad indx")
    fun test10(){
        val exp: Exception = assertThrows(PlocError::class.java){
            map.ploc["<>"]
        }
        assertEquals("Hey, buddie, in your Ploc call is mistake cause bad index", exp.message)
    }



}